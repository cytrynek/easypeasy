# The End of the Book

Hey mate, Hackauthor² again. Firstly, thank you so incredibly much for reading this book. Freedom is incredible isn't it! The original Hackauthor's book changed my life, and it's been smooth sailing ever since. Would just like to reiterate how much of a top bloke Allen Carr is, who has helped countless others escape the addiction trap.

If this hackbook helped you, you can do a couple of things to help out:

Improve the book.

:   EasyPeasy is open source and written in R Markdown. If you notice an improvement, it's very easy to change and you can find a [video guide on how to do so here]() NOT DONE YET!! 

Translate it!

:   It is impossible to express my unbelievable gratitude that people are willing to translate this book and help free others. Here's a [comprehensive video guide on setting up RStudio and publishing a translation.]() NOT DONE YET!!

Spread the word.
:   While you've already been appealed to share this book, just wanted to mention to any zoomers reading that something cool might be discussing porn with your school and getting it added into the sex education curriculum. Instead of being a TikTok fortnite zoomer memelord you could actually do something. Get off the internet and see how you can change your world for the better, not being someone else's product. 

Donate to Alexander Rhodes's legal fund.

:   The porn industry and its allies have been slandering NoFap's founder, and it's pretty messed up. While any notions of the willpower method's effectiveness have hopefully left your mind, and the book is banned from any of their platforms, it's still munted. You're already aware of how much money you're saving by quitting, lend him a helping hand. <https://nofap.com/defend-alex/>

Pre-COVID, I was in New Zealand. Did you know the best way of preventing jetlag is fasting and drinking minimal water sixteen hours before waking up? Not just for jetlag, if you've knocked your circadian rhythm out of wack for any reason, like staying up late looking at porn, it might help.

It's written that you shouldn't changed your life ***purely*** because you've stopped, and that's true. However, freedom from any addiction will allow you to build the life you didn't know you wanted.

Did you know that you can get hooked on a certain level of information? Social media is designed to resemble a slot machine. Constant bombardment of information reduces your ability to concentrate, handle stress and your mood. More insideously, it places your self worth in arbitrary value systems. But more importantly, what emotion are you feeling when you're reading this paragraph? Is it fear?

Like every social media user, you've been lured into the most sinister, subtle trap that man and nature have combined to devise (the addiction trap). Reflect, did you ever make the conscious 'positive' decision that you *needed* social media, you *needed* it permanently in your life, feeling insecure, even panic stricken without it?

Good first steps would be tracking how much time spent on ***all*** technology, uninstalling apps, and setting your phone to greyscale. Write down all the high quality offline activities you'll do in the considerable time saved, and list all the things you *have* to do on the computer (study, work, etc,), making them as optimised (and snapchat isn't one!) as possible. You have to want to do this, but as you think about it more, there's a sneaking suspicion in my mind that you'll find that you don't actually enjoy it. Slowly reintroduce the technology after a 30 day detox -- or perhaps even longer.

Meditation has been shilled previously, but you should actually do it. It's not a religious thing in any way, and it's important to separate meditation - the practice of noticing thought - from any preconceived Buddhist notions.
Don't get any kickbacks from this, but personally recommend the *Waking Up* app and course, and you get get a free month of it at the below link. Sam Harris is such a great bloke that if you can't afford it you can request a free year, no questions asked.
<https://share.wakingup.com/a13290>

Your freedom from pornography will likely open questions on relationships. If you're having dating and relationship difficulties - and even if you aren't - **huge recommend** on *Models* by Mark Manson which discusses vulnerability in relationships. Am equally happy that to not fall into the pick-up-artist or incel traps either, it's really not healthy, don't @ me.

Work out too, my go to book on the subject is *The Leangains Method* by Martin Berkhan. For males or females any age, weightlifting is pretty much the way to go for keeping yourself healthy and well.

Otherwise, if you're looking for something shorter, my favourite piece of text at current is [Exiting Modernity](https://www.meta-nomad.net/exiting-modernity/) by Meta Nomad. If you take anything from this section, it's that you should read this document. Be warned -- it's quite a confronting, but it's an enlightening experience if you let it be one.

> *"Too late isn't an age or time; too late is when fatigue leads to submission and you forget yourself completely, a potential human dissolved into nothingness*"
>
> --- Meta Nomad, *Exiting Modernity*

Ultimately these are all just suggestions. What would you like to accomplish? You're free from this awful trap and can use this gift however you wish.

The pornographic hydra has grown beyond control. While societal response has been slow, you have to be optimistic, as every person escaping accelerates the snowball. Always keep in mind what can be accomplished through focusing your attention, campaign local issues you care about, such as sustainability, sex education and others. Never complain, be proactive and ask yourself how you'll influence the world around you. Remember, when has top down revolution ever worked, change starts with you.

Cheers mate,

Hackauthor²
